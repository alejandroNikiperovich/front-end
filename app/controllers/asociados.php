<?php
    $errores = array();
    $nombre = "";
    $imagen = "";
    $descripcion = "";

    if ($_SERVER["REQUEST_METHOD"] === "POST") {
        try {
            $nombre = trim(htmlspecialchars($_POST["nombre"]));
            $descripcion = trim(htmlspecialchars($_POST["descripcion"]));
            $tiposAceptados = ["image/jpeg", "image/png", "image/gif"];
            $imagen = new File("logo", $tiposAceptados);
            $mensaje = "Datos enviados";
            $imagen->saveUploadFile(Asociado::RUTA_LOGOS);


        } catch (FileException $fileException) {
            $errores[] = $fileException->getMessage();
        }
    }

    require __DIR__ . "/../views/asociados.view.php";
?>