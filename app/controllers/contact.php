<?php
    // Inicializamos los arrays para que no de error al entrar por primera vez
    $errores = array();
    $persistencia = array(
        "firstName" => '',
        "lastName" => '',
        "email" => '',
        "subject" => '',
        "message" => ''
    );
    $resultado = $persistencia;

    if (isset($_POST["submit"])) {
        // Comprobamos que los campos obligatorios se han mandado
        if (empty($_POST["firstName"]) || empty($_POST["email"])
        || empty($_POST["subject"])) {
            $errores[] = "Last Name, Email and Subject are required";
        }

        // Comprobamos que el email es correcto
        if (filter_var($_POST["email"], FILTER_VALIDATE_EMAIL) === false) {
            $errores[] = "You have to write a valid email in the Email field";
        }

        // Si hay errores guardamos en persistencia
        if (count($errores) > 0) {
            foreach ($persistencia as $clave => &$valor) {
                if (!empty($_POST[$clave])) {
                    $valor = $_POST[$clave];
                }
            }
        // Si no hay errores guardamos los cambios
        } else {
            foreach ($resultado as $clave => &$valor) {
                if (!empty($_POST[$clave])) {
                    $valor = $_POST[$clave];
                }
            }
        }
    }

    // Require al final para que no nos de error al entrar por primera vez
    require __DIR__ . "/../views/contact.view.php";
?>