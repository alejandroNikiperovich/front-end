<?php
    try {
        $errores = array();
        $mensaje = "";
        $imagen = "";
        $descripcion = "";
        $categoriaSeleccionada = "";
        $builderImagen = new ImagenGaleriaRepository();
        $builderCategoria = new CategoriaRepository();

        if ($_SERVER["REQUEST_METHOD"] === "POST") {
            try {
                // Procesamos la descripción y guardamos el archivo subido en una variable
                FlashMessage::set("descripcion", trim(htmlspecialchars($_POST["descripcion"])));
                $descripcion = FlashMessage::get("descripcion");
                FlashMessage::unset("descripcion");

                FlashMessage::set("categoria", trim(htmlspecialchars($_POST["categoria"])));
                $categoriaSeleccionada = FlashMessage::get("categoria");
                FlashMessage::unset("categoria");

                $tiposAceptados = ["image/jpeg", "image/png", "image/gif"];
                $imagen = new File("imagen", $tiposAceptados);

                // Subimos el archivo a la galería
                $imagen->saveUploadFile(ImagenGaleria::RUTA_IMAGENES_GALERIA);

                // Copiamos el archivo desde la galería al portfolio
                $origen = ImagenGaleria::RUTA_IMAGENES_GALERIA . $imagen->getFilename();
                $destino = ImagenGaleria::RUTA_IMAGENES_PORTFOLIO . $imagen->getFilename();
                $imagen->copyFile($origen, $destino);

                // Guardamos los datos de la imagen en un nuevo registro de la BD
                $imagenGaleria = new ImagenGaleria(0, $imagen->getFilename(), $categoriaSeleccionada, $descripcion);
                $builderImagen->save($imagenGaleria);

                FlashMessage::set("mensaje", "Se ha guardado la imagen en la BBDD.");
                $mensaje = FlashMessage::get("mensaje");
                FlashMessage::unset("mensaje");

                App::get("logger")->add($mensaje);
            } catch (FileException $fileException) {
                FlashMessage::set("errores", [$fileException->getMessage()]);
            }
        }

        $imagenes = $builderImagen->findAll();
        $categorias = $builderCategoria->findAll();

    } catch (AppException $appException) {
        FlashMessage::set("errores", [$appException->getMessage()]);
    }

    $errores = FlashMessage::get("errores");
    FlashMessage::unset("errores");

    if (empty($errores)) {
        $descripcion = "";
        $categoriaSeleccionada = "";
    }
        
    require __DIR__ . "/../views/galeria.view.php";
?>