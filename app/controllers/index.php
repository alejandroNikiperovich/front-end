<?php
    try {
        $builderImagen = new ImagenGaleriaRepository();
        $imagenesBD = $builderImagen->findAll();
    } catch (AppException $appException) {
        echo $appException->getMessage();
    }

    $imagenes = array();
    for ($i = 1; $i <= 12; $i++) {
        $imagenes[$i] = $imagenesBD[$i - 1];
    }

    $asociados = array();
    for ($i = 0; $i < 3; $i++) {
        $asociados[$i] = new Asociado(
            "Asociado $i",
            "log$i.jpg",
            "descripcion_asociado$i"
        );
    };

    $muestraAsociados = extraerTres($asociados);
    
    require __DIR__ . "/../views/index.view.php";
?>