<?php
    return [
        "front-end/home" => "app/controllers/index.php",
        "front-end/about" => "app/controllers/about.php",
        "front-end/asociados" => "app/controllers/asociados.php",
        "front-end/blog" => "app/controllers/blog.php",
        "front-end/contact" => "app/controllers/contact.php",
        "front-end/galeria" => "app/controllers/galeria.php",
        "front-end/post" => "app/controllers/single_post.php"
    ];
?>