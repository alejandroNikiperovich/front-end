<?php
    // Incluímos el header y el nav
    include __DIR__ . "/partials/inicio-doc.part.php";
    include __DIR__ . "/partials/nav.part.php";
?>

<!-- Principal Content Start -->
<div id="galeria">
    <div class="container">
        <div class="col-xs-12 col-sm-8 col-sm-push-2">
            <h1>ASOCIADOS</h1>
            <hr>
            <!-- Escribimos si se ha enviado un formulario -->
            <?php if ($_SERVER["REQUEST_METHOD"] === "POST") : ?>
            <!-- Cambiamos la clase según si tenemos errores o no -->
            <div class="alert alert-<?= empty($errores) ? "info" : "danger"; ?> alert-dismissible" role="alert">
                <button type="button" class="clase" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
                <!-- Si no hay errores mostramos un mensaje -->
                <?php if (empty($errores)) : ?>
                <p><?= $mensaje ?></p>
                <!-- Si hay errores los listamos -->
                <?php else : ?>
                <ul>
                    <?php foreach($errores as $error) : ?>
                    <li><?= $error ?></li>
                    <?php endforeach; ?>
                </ul>
                <?php endif; ?>
            </div>
            <?php endif; ?>

            <!-- Formulario que se procesa en la misma página -->
            <form class="form-horizontal" action="<?= $_SERVER["PHP_SELF"] ?>" method="POST" enctype="multipart/form-data">
                <div class="form-group">
                    <div class="col-xs-12">
                        <label class="label-control">Nombre</label>
                        <input class="form-control-file" name="nombre" type="text" value="<?= $nombre ?>">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-12">
                        <label class="label-control">Logo</label>
                        <input class="form-control-file" name="logo" type="file">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-12">
                        <label class="label-control">Descripción</label>
                        <textarea class="form-control" name="descripcion" value="<?= $descripcion ?>"></textarea>
                        <button class="pull-right btn btn-lg sr-button">ENVIAR</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Principal Content End -->
<?php include __DIR__ . "/partials/fin-doc.part.php"; ?>