<?php
    // Incluímos el header y el nav
    include __DIR__ . "/partials/inicio-doc.part.php";
    include __DIR__ . "/partials/nav.part.php";
?>

<!-- Principal Content Start -->
<div id="galeria">
    <div class="container">
        <div class="col-xs-12 col-sm-8 col-sm-push-2">
            <h1>GALERÍA</h1>
            <hr>
            <!-- Escribimos si se ha enviado un formulario -->
            <?php if ($_SERVER["REQUEST_METHOD"] === "POST") : ?>
            <!-- Cambiamos la clase según si tenemos errores o no -->
            <div class="alert alert-<?= empty($errores) ? "info" : "danger"; ?> alert-dismissible" role="alert">
                <button type="button" class="clase" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
                <!-- Si no hay errores mostramos un mensaje -->
                <?php if (empty($errores)) : ?>
                <p><?= $mensaje ?></p>
                <!-- Si hay errores los listamos -->
                <?php else : ?>
                <ul>
                    <?php foreach($errores as $error) : ?>
                    <li><?= $error ?></li>
                    <?php endforeach; ?>
                </ul>
                <?php endif; ?>
            </div>
            <?php endif; ?>

            <!-- Formulario que se procesa en la misma página -->
            <form class="form-horizontal" action="galeria" method="POST" enctype="multipart/form-data">
                <div class="form-group">
                    <div class="col-xs-12">
                        <label class="label-control">Imagen</label>
                        <input class="form-control-file" name="imagen" type="file">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-12">
                        <label class="label-control">Descripción</label>
                        <!-- Tenemos persistencia de datos en este campo -->
                        <textarea class="form-control" name="descripcion"><?= $descripcion ?></textarea>
                    </div>
                </div>
                <div class="col-xs-12">
                    <label class="label-control">Categoría</label>
                    <select class="form-control" name="categoria">
                        <?php foreach ($categorias as $categoria) : ?>
                        <option value="<?= $categoria->getId()?>" <?= $categoriaSeleccionada == $categoria->getId() ? "selected" : "" ?>>
                        <?= $categoria->getNombre();?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <button class="pull-right btn btn-lg sr-button">ENVIAR</button>
            </form>

            <table>
                <tr>
                    <th>Nombre</th>
                    <th>Categoría</th>
                    <th>Descripción</th>
                    <th>Visualizaciones</th>
                    <th>Likes</th>
                    <th>Downloads</th>
                </tr>
                <?php
                    foreach ($imagenes as $imagenTabla) {
                        echo "<tr>";
                        echo "<td>" . $imagenTabla->getNombre() . "</td>";
                        echo "<td>" . $builderImagen->getCategoria($imagenTabla)->getNombre() . "</td>";
                        echo "<td>" . $imagenTabla->getDescripcion() . "</td>";
                        echo "<td>" . $imagenTabla->getNumVisualizaciones() . "</td>";
                        echo "<td>" . $imagenTabla->getNumLikes() . "</td>";
                        echo "<td>" . $imagenTabla->getNumDownloads() . "</td>";
                        echo "</tr>";
                    }
                ?>
            </table>
        </div>
    </div>
</div>
<!-- Principal Content End -->
<!-- Incluímos el footer -->
<?php include __DIR__ . "/partials/fin-doc.part.php"; ?>