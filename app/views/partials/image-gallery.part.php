            <!-- Category pictures -->
            <div id="<?= $idCategoria ?>" class="tab-pane <?php if ($idCategoria == "category1") {echo "active";}; ?>">
              <div class="row popup-gallery">
                  <?php 
                    foreach ($imagenes as $imagen) {
                      include __DIR__ . "/imagen.part.php";
                    }
                  ?>
              </div>
              <nav class="text-center">
                <ul class="pagination">
                  <li class="active"><a href="#">1</a></li>
                  <li><a href="#">2</a></li>
                  <li><a href="#">3</a></li>
                  <li><a href="#" aria-label="suivant">
                    <span aria-hidden="true">&raquo;</span>
                  </a></li>
                </ul>
              </nav>
           </div>
        <!-- End of category pictures -->