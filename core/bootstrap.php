<?php
    session_start();

    require_once "App.php";
    require_once "Request.php";
    require_once "helpers/FlashMessage.php";

    require_once "database/Connection.php";
    require_once "database/IEntity.php";
    require_once "database/QueryBuilder.php";

    require_once "entity/Asociado.php";
    require_once "entity/Categoria.php";
    require_once "entity/ImagenGaleria.php";

    require_once "exceptions/AppException.php";
    require_once "exceptions/FileException.php";
    require_once "exceptions/NotFoundException.php";
    require_once "exceptions/QueryException.php";

    require_once "repository/CategoriaRepository.php";
    require_once "repository/ImagenGaleriaRepository.php";

    require_once "utils/File.php";
    require_once "utils/MyLog.php";
    require_once "utils/utils.php";

    require_once "vendor/autoload.php";

    $config = require_once __DIR__ . "/../app/config.php";
    App::bind("config", $config);

    $logger = MyLog::load("logs/info.log");
    App::bind("logger", $logger);
?>