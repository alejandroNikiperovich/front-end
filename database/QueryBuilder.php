<?php
    abstract class QueryBuilder {
        private $connection;
        private $table;
        private $classEntity;

        public function __construct(string $table, string $classEntity) {
            $this->connection = App::getConnection();
            $this->table = $table;
            $this->classEntity = $classEntity;
        }

        // Devuelve todos los registros de una tabla $table como objetos de una clase $classEntity
        public function findAll() {
            $sql = "SELECT * FROM $this->table";
            $result = $this->executeQuery($sql);

            return $result;
        }

        // Inserta un registro en una tabla
        public function save(IEntity $entity) : void {
            try {
                // Guardamos el array con los valores de las propiedades del objeto
                $parameters = $entity->toArray();

                // Creamos la sentencia con la que insertaremos el registro
                // %s es el nombre de la tabla, las columnas a insertar y los valores de esas columnas
                // No insertamos los valores directamente, tan solo lo preparamos escribiendo :columna
                $sql = sprintf("INSERT INTO %s (%s) VALUES (%s)",
                    $this->table,
                    implode(", ", array_keys($parameters)),
                    ":" . implode(", :", array_keys($parameters))
                );

                // Preparamos la sentencia
                $statement = $this->connection->prepare($sql);

                // Ejecutamos la sentencia con los valores del array
                $statement->execute($parameters);
            } catch (PDOException $exception) {
                throw new QueryException("Error al insertar en la BBDD.");
            }
        }

        public function executeQuery(string $sql): array {
            $pdoStatement = $this->connection->prepare($sql);

            if ($pdoStatement->execute() === false) {
                throw new QueryException("No se ha podido ejecutar la consulta");
            }

            return $pdoStatement->fetchAll(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, $this->classEntity);
        }

        public function find(int $id): IEntity {
            $sql = "SELECT * FROM $this->table WHERE id=$id";
            $result = $this->executeQuery($sql);

            if (empty($result)) {
                throw new NotFoundException("No se ha encontrado el elemento con id $id");
            }

            return $result[0];
        }
    }
?>