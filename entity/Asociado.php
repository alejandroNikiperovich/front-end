<?php
    class Asociado {
        const RUTA_LOGOS = "images/index/asociados/";

        private $nombre;
        private $logo;
        private $descripcion;

        public function __construct($nombre, $logo, $descripcion) {
            $this->nombre = $nombre;
            if ($this->nombre == "") {
                throw new Exception("No has especificado un nombre");
            }

            $this->logo = $logo;
            $this->descripcion = $descripcion;
        }

        /**
         * Get the value of nombre
         */ 
        public function getNombre()
        {
                return $this->nombre;
        }

        /**
         * Get the value of logo
         */ 
        public function getLogo()
        {
                return $this->logo;
        }
        
        /**
         * Get the value of descripcion
         */ 
        public function getDescripcion()
        {
                return $this->descripcion;
        }
    }
?>