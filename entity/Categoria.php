<?php
    class Categoria implements IEntity {
        private $id;
        private $nombre;
        private $numImagenes;

        public function __construct($id = "", $nombre = "", $numImagenes = 0) {
            $this->id = $id;
            $this->nombre = $nombre;
            $this->numImagenes = $numImagenes;
        }

        public function toArray() : array {
            return [
                "nombre"=>$this->getNombre(),
                "numImagenes"=>$this->getNumImagenes()
            ];
        }

        /**
         * Get the value of id
         */ 
        public function getId()
        {
                return $this->id;
        }

        /**
         * Set the value of id
         *
         * @return  self
         */ 
        public function setId($id)
        {
                $this->id = $id;

                return $this;
        }

        /**
         * Get the value of nombre
         */ 
        public function getNombre()
        {
                return $this->nombre;
        }

        /**
         * Set the value of nombre
         *
         * @return  self
         */ 
        public function setNombre($nombre)
        {
                $this->nombre = $nombre;

                return $this;
        }

        /**
         * Get the value of numImagenes
         */ 
        public function getNumImagenes()
        {
                return $this->numImagenes;
        }

        /**
         * Set the value of numImagenes
         *
         * @return  self
         */ 
        public function setNumImagenes($numImagenes)
        {
                $this->numImagenes = $numImagenes;

                return $this;
        }
    }
?>