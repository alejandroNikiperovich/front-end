<?php
    class ImagenGaleria implements IEntity {
        const RUTA_IMAGENES_PORTFOLIO = "images/index/portfolio/";
        const RUTA_IMAGENES_GALERIA = "images/index/gallery/";

        private $id;
        private $nombre;
        private $categoria;
        private $descripcion;
        private $numVisualizaciones;
        private $numLikes;
        private $numDownloads;

        public function __construct($id = "", $nombre = "", $categoria = "", $descripcion = "", $numVisualizaciones = 0, $numLikes = 0, $numDownloads = 0) {
            $this->id = $id;
            $this->nombre = $nombre;
            $this->categoria = $categoria;
            $this->descripcion = $descripcion;
            $this->numVisualizaciones = $numVisualizaciones;
            $this->numLikes = $numLikes;
            $this->numDownloads = $numDownloads;
        }

        public function toArray() : array {
            return [
                "nombre"=>$this->getNombre(),
                "categoria"=>$this->getCategoria(),
                "descripcion"=>$this->getDescripcion(),
                "numVisualizaciones"=>$this->getNumVisualizaciones(),
                "numLikes"=>$this->getNumLikes(),
                "numDownloads"=>$this->getNumDownloads()
            ];
        }

        public function __toString() {
            return $this->getDescripcion();
        }

        public function getURLPortfolio() : string {
            return self::RUTA_IMAGENES_PORTFOLIO . $this->getNombre();
        }

        public function getURLGaleria() : string {
            return self::RUTA_IMAGENES_GALERIA . $this->getNombre();
        }

        /**
         * Get the value of nombre
         */ 
        public function getNombre()
        {
                return $this->nombre;
        }

        /**
         * Set the value of nombre
         *
         * @return  self
         */ 
        public function setNombre($nombre)
        {
                $this->nombre = $nombre;

                return $this;
        }

        /**
         * Get the value of descripcion
         */ 
        public function getDescripcion()
        {
                return $this->descripcion;
        }

        /**
         * Set the value of descripcion
         *
         * @return  self
         */ 
        public function setDescripcion($descripcion)
        {
                $this->descripcion = $descripcion;

                return $this;
        }

        /**
         * Get the value of numVisualizaciones
         */ 
        public function getNumVisualizaciones()
        {
                return $this->numVisualizaciones;
        }

        /**
         * Set the value of numVisualizaciones
         *
         * @return  self
         */ 
        public function setNumVisualizaciones($numVisualizaciones)
        {
                $this->numVisualizaciones = $numVisualizaciones;

                return $this;
        }

        /**
         * Get the value of numLikes
         */ 
        public function getNumLikes()
        {
                return $this->numLikes;
        }

        /**
         * Set the value of numLikes
         *
         * @return  self
         */ 
        public function setNumLikes($numLikes)
        {
                $this->numLikes = $numLikes;

                return $this;
        }

        /**
         * Get the value of numDownloads
         */ 
        public function getNumDownloads()
        {
                return $this->numDownloads;
        }

        /**
         * Set the value of numDownloads
         *
         * @return  self
         */ 
        public function setNumDownloads($numDownloads)
        {
                $this->numDownloads = $numDownloads;

                return $this;
        }

        /**
         * Get the value of id
         */ 
        public function getId()
        {
                return $this->id;
        }

        /**
         * Get the value of categoria
         */ 
        public function getCategoria()
        {
                return $this->categoria;
        }

        /**
         * Set the value of categoria
         *
         * @return  self
         */ 
        public function setCategoria($categoria)
        {
                $this->categoria = $categoria;

                return $this;
        }
    }
?>