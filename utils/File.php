<?php
    class File {
        private $file;
        private $fileName;

        public function __construct(string $fileName, array $arrTypes) {
            $this->file = $_FILES[$fileName];
            $this->fileName = $this->file["name"];

            if ($this->file["name"] == "") {
                throw new FileException("Debes especificar un fichero");
            }

            if ($this->file["error"] !== UPLOAD_ERR_OK) {
                switch ($this->file["error"]) {
                    case UPLOAD_ERR_INI_SIZE:
                    case UPLOAD_ERR_FORM_SIZE:
                        throw new FileException("El archivo es demasiado grande");
                    case UPLOAD_ERR_PARTIAL:
                        throw new FileException("El archivo no se ha subido por completo");
                    default:
                        throw new FileException("Hubo un problema al subir el archivo");
                        break;
                }
            }

            if (in_array($this->file["type"], $arrTypes) === false) {
                throw new FileException("El tipo de archivo no está soportado");
            }
        }

        public function saveUploadFile($rutaDestino) {
            if (is_uploaded_file($this->file["tmp_name"])) {
                if (is_file($rutaDestino . $this->fileName)) {
                    $this->file["name"] = time() . "_" . $this->file["name"];
                    $this->fileName = $this->file["name"];
                }

                if (!move_uploaded_file($this->file["tmp_name"], $rutaDestino . $this->file["name"])) {
                    throw new FileException("No se ha podido mover el fichero al destino especificado");
                }
            } else {
                throw new FileException("El archivo no se ha subido mediante un formulario");
            }
        }

        public function copyFile($rutaOrigen, $rutaDestino) {
            if (is_file($rutaOrigen)) {
                if (!is_file($rutaDestino)) {
                    if (!copy($rutaOrigen, $rutaDestino)) {
                        throw new FileException("No se ha podido copiar el fichero");
                    }
                } else {
                    throw new FileException("El fichero $rutaDestino ya existe");
                }
            } else {
                throw new FileException("No existe el fichero $rutaOrigen");
            }
        }

        /**
         * Get the value of fileName
         */ 
        public function getFileName()
        {
                return $this->fileName;
        }
    }
?>