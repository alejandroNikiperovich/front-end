<?php
    function esActiva ($opcion) {
        $url = $_SERVER["REQUEST_URI"];
        if (strpos($url, $opcion) !== false) {
            return true;
        } else {
            return false;
        }
    }
    
    function extraerTres($array) {
        shuffle($array);
        $arrayPartido = array_chunk($array, 3);
        return $arrayPartido[0];
    }
?>